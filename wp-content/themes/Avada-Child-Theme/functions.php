<?php

function theme_enqueue_styles() {

  // Added "style.css" to load AFTER "child-theme.min.css", this makes it easy for quick edits to css without running sass on staging server
  wp_enqueue_style( 'child-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css', array( 'avada-stylesheet' ) );
  wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'avada-stylesheet' ) );
  // child-theme.min.js runs required stuff, then vendor.js, then app.js
  wp_enqueue_script( 'child-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array() );

}
// Priority 20 is set so custom css / js loads after (almost) all other files
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles', 20);

// Taken from original Avada Child Theme. Not sure what this does?
function avada_lang_setup() {
	$lang = get_stylesheet_directory() . '/languages';
	load_child_theme_textdomain( 'Avada', $lang );
}
add_action( 'after_setup_theme', 'avada_lang_setup' );
