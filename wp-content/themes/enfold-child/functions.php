<?php

/*
* Add your own functions here. You can also copy some of the theme functions into this file.
* Wordpress will use those functions instead of the original functions then.
*/

// Priority 20 is set so custom css / js loads after (almost) all other files
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles', 20 );
function theme_enqueue_styles() {

	// Get the theme data
	$the_theme = wp_get_theme();

		// Add Font Awesome
		wp_enqueue_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );

    // Added "style.css" to load AFTER "child-theme.min.css", this makes it easy for quick edits to css without running sass on staging server
    wp_enqueue_style( 'child-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_style( 'main-style', get_stylesheet_directory_uri() . '/style.css', array(), $the_theme->get( 'Version' ) );

    wp_enqueue_script( 'child-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $the_theme->get( 'Version' ), true );
}



// Add extra Google Fonts
add_filter( 'avf_google_heading_font',  'avia_add_heading_font');
// Header Options
function avia_add_heading_font($fonts)
{
$fonts['Convergence'] = 'Convergence:400';
return $fonts;
}

add_filter( 'avf_google_content_font',  'avia_add_content_font');
// Content Options
function avia_add_content_font($fonts)
{
$fonts['Ledger'] = 'Ledger:400';
return $fonts;
}
