<?php
/**
 * Template Name: Splash Page
 *
**/
 ?>

 <!DOCTYPE html>
 <html lang="en">
   <head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
     <title><?php echo bloginfo('name') ?></title>

     <!-- Bootstrap -->
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
     <!-- Google Fonts -->
     <link href="https://fonts.googleapis.com/css?family=Merriweather|Montserrat" rel="stylesheet">

     <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
       <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
       <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     <![endif]-->
   </head>
   <body>


<style media="screen">
  .san-serif {
    font-family: 'Montserrat', sans-serif;
  }
  .serif {
    font-family: 'Merriweather', serif;
  }
</style>


     <div class="container">
       <div class="row" style="margin-top: 25vh">
         <div class="col-md-3"></div>
         <div class="col-md-6 text-center">
           <!-- Paste url of image -->
           <img class="img-responsive" src="<?php echo site_url() . '' ?>" alt="">
           <hr>
           <!-- Add text here -->
         </div>
         <div class="col-md-3"></div>
       </div>
     </div>


   </body>
 </html>
