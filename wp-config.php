<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'master');

/** MySQL database username */
define('DB_USER', 'master');

/** MySQL database password */
define('DB_PASSWORD', 'master');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'rp<B$?hB;XP5q4v)NT:9!J,R%5qi)la*Y6$ajSQEeDA8nUNv|r2RCnFiQ4rRs~wH');
define('SECURE_AUTH_KEY',  'R+lg|hjYD/4^^~5Y/ tdHp87aK~+.Y/o8kH&R-r2f9mR;5ZC(DSzQ5:oDG8XdRFI');
define('LOGGED_IN_KEY',    'VAX+vp;G:Aqqxr4@Yx~GCE4<X}47 38kw0MLUk3,{p&j hQtIr,2L1F,*%^9}|TF');
define('NONCE_KEY',        'dagbP5eBFRh2dX8)f)w+PY{T*4wra>Azy@AqZD~Of:](c^2lgqG9ru8@Nu:Q?&+:');
define('AUTH_SALT',        '.pq*7kl`O5/=#&_Qv6!+Gc*ptnL6y D>)tFpG[(~8aytzl>JLOuGb)ww $O|UpYE');
define('SECURE_AUTH_SALT', ',pI3:?eE<*m3e#Y>w[i148-p22TV_wR/33TJQjuYBNb_72V>,agj^Tg{*>WW%di!');
define('LOGGED_IN_SALT',   'M6Z7lgB3d~T82OQ)#7^j(B-wew6r4vghNwR[*Ie,[5i.xiZ)d!QD4ZQ2r%RCa&~<');
define('NONCE_SALT',       ')=_o~p?c~`s]po;{;rW>jqI#ZDhI.(Fhf)YS<MSJbTWY4tm*ZD0f~qFs[]~VYPmm');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
